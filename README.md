# 前端技术框架介绍

现有的前端技术框架主要有三个方向：

- 小程序
- vue 技术栈
- react 技术栈

下面简单介绍一下这些技术栈和相关的一些背景

### 使用 es6 语法

es6 的基础教程推荐阮一峰的这个文档[http://es6.ruanyifeng.com/](http://es6.ruanyifeng.com/)

里面详细的介绍了 es6 的基础语法


### 使用 babel

现有一些浏览器并不支持原生的 es6 语法，所以需要通过 `babel` 进行编译，通过配置 babel 可以将 es6 语法编译为运行环境可以执行的目标语法。
babel 的官方网站在这里 [https://babeljs.io/](https://babeljs.io/)，里面比较详细的介绍了 babel 的各个方面。

在 babel 中，除了 `babel-core`外，我们经常使用的有两个部分，分别是 `preset` 和 `plugin`。
其中 `preset`可以理解为 `babel` 将要转移的源语法;
`plugin`就是插件，通过 plugin 机制，可以转义一些还没有进入语法标准，但是事实存在的语法糖。

`babel`推荐使用 `.babelrc`进行配置，下面是一个常用的 `.babelrc`，可以参考这个写一个自己的 `babel`配置文件

```
{
    "presets": ["react", "env", "stage-0"],
    "plugins": [
        "syntax-decorators",
        "transform-decorators-legacy",
        "transform-decorators",
        "transform-regenerator"
    ]
}

```

通常情况下，我们不会单独使用 babel（除非正在开发的是个纯 js 业务的逻辑）。
一般都会使用 `babel`作为整个编译流程的一部分，然后通过`webpack`来构建整个前端的编译流程。


### 使用 webpack

`webpack`的主要作用是整合前端开发过程中的各种资源，通过 `webpack`，我们可以把 js 文件当做项目的入口，
使用各种 `loader`，会将所有引用到的资源最终打包到一起。

`webpack`中比较重要的概念是 `loader` ，`loader`就是针对不同类型的文件编译器。

下面是一份常用的 `webpack`配置，比较长，没有耐心的话，可以看这里 [webpack-scripts](https://gitlab.com/tobeyouth/webpack-scripts)


### vue

相关文档看这里：[vue](https://cn.vuejs.org/index.html)
在 training 项目中主要尝试了

```
vue + vuex + vue-router
```

的技术栈。

使用 vue 推荐直接使用官方的 [vue-cli](https://github.com/vuejs/vue-cli)，方便又省心。


### react

相关文档看这里：[react](https://reactjs.org/)

比较常用的技术栈是：

```
react + redux + react-redux + (redux-thunk/redux-saga)
```


### 小程序

小程序相关的文档看这里：[小程序简易教程](https://developers.weixin.qq.com/miniprogram/dev/)

小程序需要使用官方的开发工具开发，支持 es6 语法，开发工具自带了编译功能，所以不需要再自己写编译脚本了。


